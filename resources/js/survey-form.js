var PageTransitions = (function () {

  var $main = $('#survey-form'),
    $pages = $main.children('div.pt-page'),
    pagesCount = $pages.length,
    current = 0,
    isAnimating = false,
    endCurrPage = false,
    endNextPage = false,
    animEndEventNames = {
      'WebkitAnimation': 'webkitAnimationEnd',
      'OAnimation': 'oAnimationEnd',
      'msAnimation': 'MSAnimationEnd',
      'animation': 'animationend'
    },
    // animation end event name
    animEndEventName = animEndEventNames[Modernizr.prefixed('animation')],
    // support css animations
    support = Modernizr.cssanimations;

  function init() {

    $("#dob").datepicker({
      format: 'dd/mm/yyyy',
      endDate: Date.now(),
    });

    $pages.each(function () {
      var $page = $(this);
      $page.data('originalClassList', $page.attr('class'));
    });

    $pages.eq(current).addClass('pt-page-current');

    // Handle navigate page
    updateNavState();
    $main.on('input', function () {
      updateNavState();
    })

    $.each($(".emn-submit"), function (index) {
      $(this).on('click', () => {
        highlightOnePage($(this).data('current-index'))
      })
    })
$(document).ready(function() {
                $("#kota").chained("#provinsi");
                $("#kecamatan").chained("#kota");
            });
    // Handle radio select other
    $("input[type='radio']").change(function (event) {
      let inputName = event.target.name;
      let inputEl = $("input[name='" + inputName + "']:checked")
      if (inputEl.data('target')) {
        if (inputEl.val() === 'Other') {
          $('#' + inputEl.data('target')).removeClass('d-none');
          $('#' + inputEl.data('target')).prop('required', true);
          $('#' + inputEl.data('target')).attr('disabled', false);
        } else {
          $('#' + inputEl.data('target')).addClass('d-none');
          $('#' + inputEl.data('target')).prop('required', false);
          $('#' + inputEl.data('target')).attr('disabled', true);
        }
      }
      updateNavState();
    })

    // Handle Checkbox select Other
    $("input[type='checkbox'][value='Other']").change(function (event) {
      let inputEl = $(event.target)
      if (inputEl.data('target')) {
        if (inputEl.is(':checked')) {
          $('#' + inputEl.data('target')).removeClass('d-none');
          $('#' + inputEl.data('target')).prop('required', true);
          $('#' + inputEl.data('target')).attr('disabled', false);
        } else {
          $('#' + inputEl.data('target')).addClass('d-none');
          $('#' + inputEl.data('target')).prop('required', false);
          $('#' + inputEl.data('target')).attr('disabled', true);
        }
      }
      updateNavState();
    })

  }

  function updateNavState() {
    for (let i = 0; i < pagesCount; i++) {
      let navs = $(`.emn-nav[data-target-index="${i}"]`)
      if (validateToPage(i)) {
        $.each(navs, function () {
          $(this).off('click')
          $(this).on('click', () => changePage(i))
          $(this).addClass('step__item--clickable')
        })
      } else {
        $.each(navs, function () {
          $(this).off('click')
          $(this).on('click', () => highlightToPage(i))
          $(this).removeClass('step__item--clickable')
        })
      }
    }
  }

  function getFields(page) {
    // return []
    switch (page) {
      case 0:
        return ['name;i', 'dob;i', 'email;i', 'phone;i', /*'ig-account;i',*/ 'provinsi;i','kota;i','money-spent;r', 'what-you-do;r', 'what-you-do-other;i', 'skin-type;r', 'skin-concern[];r', 'skin-concern[];i', 'skin-tone;r', 'undertone;r', 'how-you-know;r', 'how-you-know-other;i', 'which-do-you-have[];r', 'which-do-you-have[];i', 'must-have[];r', 'must-have[];i' ];
      // case 1:
      //   return ['skin-type;r', 'skin-concern[];r', 'skin-concern[];i'];
      // case 2:
      //   return ['skin-tone;r', 'undertone;r'];
      // case 3:
      //   return ['how-you-know;r', 'how-you-know-other;i', 'which-do-you-have[];r', 'which-do-you-have[];i'];
      // case 4:
      //   return ['must-have[];r', 'must-have[];i', /*'product-next-launch;i'*/];
      // default:
        return [];
    }
  }

  // Check if a field is valid
  function validateField(field) {
    let [name, type] = field.split(';');
    if (type === 'i') {
      if ($(`[name='${name}'][type!="radio"][type!="checkbox"]`).is(':invalid')) return false;
    } else if (type === 'r') {
      if (!$(`[name='${name}']`).is(':checked')) return false;
    } else {
      return false;
    }
    return true;
  }

  // Check if all fields on the current page is valid
  function validateOnePage(page) {
    let fields = getFields(page);
    for (let field of fields) {
      if (!validateField(field)) return false;
    }
    return true;
  }

  // Highlight invalid field
  function highlightOnePage(page) {
    let fields = getFields(page);
    for (let field of fields) {
      let [name, type] = field.split(';');
      if (!$(`[name='${name}']`).is(':visible')) continue;
      if (!validateField(field)) {
        $(`[name='${name}']`).parents('.form').addClass('form--invalid');
      } else {
        $(`[name='${name}']`).parents('.form').removeClass('form--invalid')
      }
    }
  }

  // Highlight all fields until a specific page
  // When user click navigation
  function highlightToPage(page) {
    for (let i = 0; i < page; i++) {
      highlightOnePage(i)
    }
    return true;
  }

  // Validate all fields until a specific page
  // When user click navigation
  function validateToPage(page) {
    for (let i = 0; i < page; i++) {
      if (!validateOnePage(i)) {
        return false
      }
    }
    return true;
  }

  function changePage(nextPage) {

    if (nextPage > pagesCount - 1) {
      nextPage = pagesCount - 1;
    }

    if (nextPage < 0) {
      nextPage = 0;
    }

    if (isAnimating) {
      return false;
    }

    if (current == nextPage) {
      return false;
    }

    isAnimating = true;

    let animation = 1;
    if (current > nextPage) {
      animation = 2;
    }

    var $currPage = $pages.eq(current);
    current = nextPage;
    var $nextPage = $pages.eq(current).addClass('pt-page-current'),
      outClass = '', inClass = '';

    switch (animation) {

      case 1:
        outClass = 'pt-page-moveToLeft';
        inClass = 'pt-page-moveFromRight';
        break;
      case 2:
        outClass = 'pt-page-moveToRight';
        inClass = 'pt-page-moveFromLeft';
        break;
    }

    $currPage.addClass(outClass).on(animEndEventName, function () {
      $currPage.off(animEndEventName);
      endCurrPage = true;
      if (endNextPage) {
        onEndAnimation($currPage, $nextPage);
      }
    });

    $nextPage.addClass(inClass).on(animEndEventName, function () {
      $nextPage.off(animEndEventName);
      endNextPage = true;
      if (endCurrPage) {
        onEndAnimation($currPage, $nextPage);
      }
    });

    if (!support) {
      onEndAnimation($currPage, $nextPage);
    }

    $.each($(".emn-nav"), function (index) {
      $(this).removeClass('step__item--active')
    })

    $(".emn-nav").eq(current).addClass('step__item--active')

  }

  function onEndAnimation($outpage, $inpage) {
    endCurrPage = false;
    endNextPage = false;
    resetPage($outpage, $inpage);
    isAnimating = false;
  }

  function resetPage($outpage, $inpage) {
    $outpage.attr('class', $outpage.data('originalClassList'));
    $inpage.attr('class', $inpage.data('originalClassList') + ' pt-page-current');
  }

  init();

  return { init: init };

})();
