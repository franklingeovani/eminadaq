@extends('layouts.default')

<style>
  html,
  body {
    height: 100%;
  }
</style>

@section('content')
  <main class="voucher-container">
    <div class="container-narrow voucher-area">
      <div class="voucher">
        <a href="/">
          <img class="voucher__logo" src="/assets/logo.png" alt="Emina" height="28px">
        </a>
        <h1 class="display3" style="text-align: center; margin-bottom: 64px;">Thank you <br>for participating!</h1>
        <p class="plarge" style="max-width: 80%; text-align: center; margin-bottom: 24px;">Here's your voucher code:</p>
        <div class="voucher__code">SRVY101</div>
        <a href="/" class="button button--small button--primaryAlt" style="margin-bottom: 24px; display: flex; align-items: center;">
          <div style="width: 24px; height: 24px; margin-right: 8px;">
            @include('icons.home')
          </div>
          <span style="display: block; height: 15px;">
            Back to Homepage
          </span>
        </a>
        <a href="#"><p class="voucher-terms pmedium">Terms & Conditions</p></a>
      </div>
    </div>
    @include('components.stickers.stickers')
  </main>
@endsection
