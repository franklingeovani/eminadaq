@extends('layouts.default')

<style>
  html,
  body { height: 100%; }
</style>

@section('content')
  <section class="voucher-container">
    <div class="container-narrow voucher-area">
      <header class="voucher__header">
        <a href="/"><img class="voucher__logo" src="/assets/logo.png" alt="Emina" height="40px"></a>
      </header>
      <section class="voucher">
        <h1 class="display2 voucher__title">Thank you <br>for participating!</h1>
        <div class="voucher__support">
          <p class="plarge keep-supporting-emina">
            Keep supporting emina
          </p>
          <img class="support__brush" src="/assets/brush.png" alt="Brush">
        </div>
      </section>
      <footer  style="margin-bottom: 128px">
        <a href="/" class="button button--small button--primary" style="display: flex; align-items: center;">
          <div style="width: 24px; height: 24px; margin-right: 8px;">
            @include('icons.home')
          </div>
          <span style="display: block; height: 15px;">
            Back to Homepage
          </span>
        </a>
      </footer>
    </div>
    @include('components.stickers.stickers')
    @include('components.stickers.stickers-voucher')
  </section>
@endsection

