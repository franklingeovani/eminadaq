<header class="question">
  <div class="mobile-step">
    <div class="heading6" style="height: 18px;">
      {{ $step }}
    </div>
  </div>
  <h1 class="display3">
    {{ $title }}
  </h1>
  @if(isset($subtitle))
    <span class="plarge" style="display: block; padding-top: 24px;">
      {{ $subtitle }}
    </span>
  @endif

</header>
