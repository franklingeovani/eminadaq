<footer class="main-footer">
  <nav class="container-narrow main-footer__area">
    <a data-target-index="{{$currentpage - 1}}" class="heading6 emn-nav pl-2">PREV</a>
    @if(isset($submit) && $submit)
      <input type="submit" value="SUBMIT" class="emn-submit heading6 form-field--submit" data-current-index="{{$currentpage}}"/>
    @else
      <a  data-target-index="{{$currentpage + 1}}" class="heading6 emn-nav pr-2">NEXT</a>
    @endif
  </nav>
  @include('components.stickers.stickers-footer')
</footer>
