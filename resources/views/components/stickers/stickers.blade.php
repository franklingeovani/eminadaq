<div class="bg-sticker bg-sticker--product @isset($form) {{ "bg-sticker--form" }} @endisset">
  <img src="/assets/stickers/product.svg" alt="product sticker">
</div>
<div class="bg-sticker bg-sticker--bif @isset($form) {{ "bg-sticker--form" }} @endisset">
  <img src="/assets/stickers/bif-3.svg" alt="beauty is fun sticker">
</div>
<div class="bg-sticker bg-sticker--facemask @isset($form) {{ "bg-sticker--form" }} @endisset">
  <img src="/assets/stickers/p07.svg" alt="blush-on sticker">
</div>
<div class="bg-sticker bg-sticker--lipstick @isset($form) {{ "bg-sticker--form" }} @endisset">
  <img src="/assets/stickers/lipstick-both.svg" alt="lipstick sticker">
</div>
