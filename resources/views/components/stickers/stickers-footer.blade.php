<div class="sticker-mobile--brush sticker-mobile">
  <img src="/assets/stickers/brush.svg" alt="brush-on sticker">
</div>
<div class="sticker-mobile--lipstick sticker-mobile">
  <img src="/assets/stickers/lipstick-both.svg" alt="lipstick sticker">
</div>
