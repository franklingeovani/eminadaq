@extends('layouts.base')

@section('sidebar')

<div class="nav-wrapper">
  <ul class="nav flex-column">
    <li class="nav-item">
      <a class="nav-link active" href="/dashboard">
        <i class="material-icons">edit</i>
        <span>Emina Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
  <a class="nav-link" href="http://daq.makeoverforall.com/dashboard">
    <i class="material-icons">edit</i>
    <span> Make Over Dashboard</span>
  </a>
</li>
  </ul>
</div>

@endsection



@section('content')

<div class="main-content-container container-fluid px-4">
  <!-- Page Header -->
  <div class="page-header row no-gutters py-4">
    <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
      <span class="text-uppercase page-subtitle">Dashboard</span>
      <h3 class="page-title">Dashoard Overview</h3>
    </div>
  </div>
  <!-- End Page Header -->
  <!-- Small Stats Blocks -->
  <div class="row">
    <div class="col-lg col-md-6 col-sm-6 mb-4">
      <div class="stats-small stats-small--1 card card-small">
        <div class="card-body p-0 d-flex">
          <div class="d-flex flex-column m-auto">
            <div class="stats-small__data text-center">
              <span class="stats-small__label text-uppercase">New Contact</span>
              <h6 class="stats-small__value count my-3">{{$count_recent_contact}}</h6>
            </div>
            <div class="stats-small__data">
              <!-- <span class="stats-small__percentage stats-small__percentage--increase">4.7%</span> -->
            </div>
          </div>
          <canvas height="120" class="blog-overview-stats-small-1"></canvas>
        </div>
      </div>
    </div>
    <div class="col-lg col-md-6 col-sm-6 mb-4">
      <div class="stats-small stats-small--1 card card-small">
        <div class="card-body p-0 d-flex">
          <div class="d-flex flex-column m-auto">
            <div class="stats-small__data text-center">
              <span class="stats-small__label text-uppercase">Total Contact</span>
              <h6 class="stats-small__value count my-3">{{$count_contact}}</h6>

            </div>
            <div class="stats-small__data">
              <!-- <span class="stats-small__percentage stats-small__percentage--increase">12.4%</span> -->
            </div>
          </div>
          <canvas height="120" class="blog-overview-stats-small-2"></canvas>
        </div>
      </div>
    </div>
    <div class="col-lg col-md-4 col-sm-6 mb-4">
      <div class="stats-small stats-small--1 card card-small">
        <div class="card-body p-0 d-flex">
          <div class="d-flex flex-column m-auto">
            <div class="stats-small__data text-center">
              <span class="stats-small__label text-uppercase">Verified Contact</span>
              <h6 class="stats-small__value count my-3">{{$count_verified_contact}}</h6>
            </div>
            <div class="stats-small__data">
              <!-- <span class="stats-small__percentage stats-small__percentage--decrease">3.8%</span> -->
            </div>
          </div>
          <canvas height="120" class="blog-overview-stats-small-3"></canvas>
        </div>
      </div>
    </div>
  </div>
  <!-- End Small Stats Blocks -->
  <div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
          <div class="card card-small">
            <div class="card-header border-bottom">
              <h6 class="m-0">Search Consumer</h6>
            </div>
            <div class="card-header form-group">
              <form action="/dashboard/emina/search" method="GET">
                <div class="row">
                  <div class="col-md-12">
                    <input type="text" name="search" id="search" class="form-control" placeholder="Search . . . ." >
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12" style="text-align:center; margin-top:15px;">
                    <input type="submit" name="submit" class="btn btn-info" >
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

    <!-- Users Stats -->
    @if(Session::get('search') == TRUE)
    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
      <div class="card card-small">
        <div class="card-header border-bottom">
          <h6 class="m-0">Consumer's Search Result</h6>
        </div>
        <div class="card-header form-group">
                    <div class="row">
                      @if(count($contacts) > 0)
                        @foreach($contacts as $consumer)
                          @if($consumer->is_verified == 0)
                            <div class="col-md-12" >
                              <label type="text" style="background-color:#F8B43D;" name="search" id="search" class="form-control"><b>{{$consumer->email}}</b></label>
                            </div>
                          @else
                            <div class="col-md-12" >
                              <label type="text" style="background-color:#87D37C;" name="search" id="search" class="form-control"><b>{{$consumer->email}}</b></label>
                            </div>
                          @endif
                        @endforeach
                      @endif
                    </div>
                </div>
              </div>
        </div>
        @endif

    <!-- End Users Stats -->

    <!-- Top Referrals Component -->
    <!-- End Top Referrals Component -->


    <!-- Users By Device Stats -->
    <div class="col-lg-3 col-md-6 col-sm-12 mb-4">
      <div class="card card-small h-100">
        <div class="card-header border-bottom">
          <h6 class="m-0">Users by Verification</h6>
        </div>
        <div class="card-body d-flex py-0">

          <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
          <script src="https://code.highcharts.com/highcharts.js"></script>
          <script src="https://code.highcharts.com/modules/exporting.js"></script>
          <script src="https://code.highcharts.com/modules/export-data.js"></script>

          <div id="container" class="col-lg-12 col-md-12 col-sm-12 mb-4"></div>

          <script type="text/javascript">
          $(function () {
            var verified = <?php echo $count_verified_contact; ?>;
            var unverified = <?php echo $count_unverified_contact; ?>;

          Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Share of verified contacts'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Status',
                colorByPoint: true,
                data: [{
                    name: 'Verified',
                    y: verified,
                    sliced: true,
                    selected: true
                }, {
                    name: 'Unverified',
                    y: unverified
                }]
            }]
          })
        });
          </script>

        </div>
        <div class="card-footer border-top">
          <div class="row">
            <div class="col">
              <select class="custom-select custom-select-sm" style="max-width: 130px;">
                <option selected>Last Week</option>
                <option value="1">Today</option>
                <option value="2">Last Month</option>
                <option value="3">Last Year</option>
              </select>
            </div>
            <div class="col text-right view-report">
              <a href="#">Full report &rarr;</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Users By Device Stats -->

    <!-- Users By Occupation Stats -->
    <div class="col-lg-3 col-md-6 col-sm-12 mb-4">
      <div class="card card-small h-100">
        <div class="card-header border-bottom">
          <h6 class="m-0">Users by Occupation</h6>
        </div>
        <div class="card-body d-flex py-0">

          <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
          <script src="https://code.highcharts.com/highcharts.js"></script>
          <script src="https://code.highcharts.com/modules/exporting.js"></script>
          <script src="https://code.highcharts.com/modules/export-data.js"></script>

          <div id="container_occupation" class="col-lg-12 col-md-12 col-sm-12 mb-4"></div>

          <script type="text/javascript">
          $(function () {
            var School = <?php echo $count_occupation_school; ?>;
            var College = <?php echo $count_occupation_college; ?>;
            var Work = <?php echo $count_occupation_work; ?>;

          Highcharts.chart('container_occupation', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Share of occupation'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Status',
                colorByPoint: true,
                data: [{
                    name: 'Work',
                    y: Work,
                    sliced: true,
                    selected: true
                }, {
                    name: 'College',
                    y: College
                }, {
                    name: 'School',
                    y: School
                }]
            }]
          })
        });
          </script>

        </div>
        <div class="card-footer border-top">
          <div class="row">
            <div class="col">
              <select class="custom-select custom-select-sm" style="max-width: 130px;">
                <option selected>Last Week</option>
                <option value="1">Today</option>
                <option value="2">Last Month</option>
                <option value="3">Last Year</option>
              </select>
            </div>
            <div class="col text-right view-report">
              <a href="#">Full report &rarr;</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Users By Occupation Stats -->

  </div>
  </div>
</div>

@endsection
