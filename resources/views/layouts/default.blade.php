<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Emina</title>
  <link rel="stylesheet" href="{{ mix('/css/survey-form.css') }}">
  <link rel="shortcut icon" type="image/png" href="/assets/logomark.png"/>
  <style>
    html,
    body {
      height: 100%;
    }
  </style>
  @stack('component-styles')
</head>
<body class="Emina__Survey">
  @yield('content')
  @stack('component-scripts')
</body>
</html>
