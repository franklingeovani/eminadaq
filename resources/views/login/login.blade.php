<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Untitled</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Actor">
    <link rel="stylesheet" href="{{ secure_asset('css/backend_css/styles.min.css') }}">
</head>

<body style="width: 100%;height: 100vh;">



    <!-- Start: Login Form Clean -->
    <div class="login-clean" style="width: 100%;height: 100vh;">

      @if(Session::has('flash_message_success_logout'))
      <div class=" alert alert-success alert-block alert-dismissible fade show text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <strong>{!! session('flash_message_success_logout') !!}</strong>
      </div>
      @endif
      @if(Session::has('flash_message_warning'))
      <div class=" alert alert-warning alert-block alert-dismissible fade show text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <strong>{!! session('flash_message_warning') !!}</strong>
      </div>
      @endif

        <h1 class="text-center" id="LoginTitle" style="font-family: Actor, sans-serif;font-size: 51px;margin-top: 100px;">Emina Dashboard<br><br></h1>

        <form method="post" action="/login" style="width: center;">{{ csrf_field() }}
            <h2 class="sr-only">Login Form</h2>

            <div class="illustration"><i class="icon ion-ios-navigate"></i></div>
            <div class="form-group"><input class="form-control" type="email" name="email" placeholder="Email"></div>
            <div class="form-group"><input class="form-control" type="password" name="password" placeholder="Password"></div>

            <div class="">
              @if(Session::has('flash_message_error'))
              <div class=" alert alert-danger alert-block alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                <strong>{!! session('flash_message_error') !!}</strong>
              </div>
              @endif
            </div>


            <div class="form-group"><input class="btn btn-primary btn-block" value="Login" type="submit"></div>
            <a href="#" class="forgot">Forgot your email or password?</a>
        </form>

    </div>
    <!-- End: Login Form Clean -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{ secure_asset('js/backend_js/extras.1.1.0.min.js') }}"></script>
    <script src="{{ secure_asset('js/backend_js/shards-dashboards.1.1.0.min.js') }}"></script>
    <script src="{{ secure_asset('js/backend_js/app/app-blog-overview.1.1.0.js') }}"></script>
</body>

</html>
