@extends('layouts.default')

@section('content')
  <main class="home-container">
    <div class="container-narrow home-area">
      <img src="/assets/logo.png" alt="Emina" style="position: absolute; top: 48px;">
      <h1 class="display1" style="margin-bottom: 24px;">Hi, there!</h1>
      <p class="plarge" style="max-width: 80%; text-align: center; margin-bottom: 64px;">
        Let us know more about you!
      </p> 
      <a class="button button--primary button--large" href="/form">
        <span style="font-size: 19px; height: 16px; display: block;">Let's Get Started!</span>
      </a>
      @include('components.stickers.stickers-footer')
      @include('components.stickers.stickers-footer--additional')
    </div>
    @include('components.stickers.stickers', ['form' => true])
  </main>
@endsection
