@component('components.navbar.navbar-mobile')
@endcomponent

<div class="container-narrow" style="padding-top: 48px; padding-bottom: 48px;">

  <div class="container-narrow">

  @question([
    'step' => 'Step 2.1',
    'title' => 'What\'s your skin type?',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $skin_types = [
          "normal" => [ "value" => "Normal", "icon" => "skin-type-normal" ],
          "oily" => [ "value" => "Oily", "icon" => "skin-type-oily" ],
          "dry" => [ "value" => "Dry", "icon" => "skin-type-dry" ],
          "combination" => [ "value" => "Combination", "icon" => "skin-type-combined" ]
        ]
      @endphp

      @foreach ($skin_types as $key => $data)
        <div class="col-md-3 col-sm-4 col-6 form-field--choices">
          <input type="radio" name="skin-type" id="skin-type-{{$key}}" value="{{$data['value']}}" class="form-field--radio" required/>
          <label class="choice-icon__container" for="skin-type-{{$key}}">
            <div class="choice-icon">
              @include("icons.".$data['icon'])
            </div>
            {{$data['value']}}
          </label>
        </div>
      @endforeach

    </div>
  </div>

  @question([
    'step' => 'Step 2.2',
    'title' => 'What are your skin concerns?',
    'subtitle' => 'You can choose more than one option'
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $skin_concerns = [
          "acne" => [ "value" => "Acne", "icon" => "skin-concern-acne" ],
          "acne-scars" => [ "value" => "Acne Scars", "icon" => "skin-concern-acne-scars" ],
          "large-pores" => [ "value" => "Large Pores", "icon" => "skin-concern-large-pores" ],
          "dull-skin" => [ "value" => "Dull Skin", "icon" => "skin-concern-dull" ],
          "sensitive" => [ "value" => "Sensitive", "icon" => "skin-concern-sensitive" ],
          "uneven-skin-tone" => [ "value" => "Uneven Skin Tone", "icon" => "skin-concern-uneven" ],
          "black-white-head" => [ "value" => "Black/White Head", "icon" => "skin-concern-blackhead" ],
          "other" => [ "value" => "Other", "icon" => "plus" ]
        ]
      @endphp

      @foreach ($skin_concerns as $key => $data)
        <div class="col-md-3 col-sm-4 col-6 form-field--choices">
          <input type="checkbox" name="skin-concern[]" id="skin-concern-{{$key}}" value="{{$data['value']}}" class="form-field--radio"
            data-target="skin-concern-other-input"/>
          <label class="choice-icon__container" for="skin-concern-{{$key}}">
            <div class="choice-icon">
              @include("icons.".$data['icon'])
            </div>
            {{$data['value']}}
          </label>
        </div>
      @endforeach

      <div class="col-sm-12">
          <input
            type='text'
            style="padding: 16px 0;"
            id="skin-concern-other-input"
            class="d-none form-field--text"
            name="skin-concern[]"
            autofocus="true"
            placeholder="Please specify your skin concern..."
            disabled />
      </div>

    </div>
  </div>

  @question([
    'step' => 'Step 5.1',
    'title' => 'What are your must-have beauty products in your pouch?',
    'subtitle' => 'You can choose more than one option',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $must_have = [
          "lip-cream" => [ "value" => "Lip Cream", "img" => "assets/products/creammate.png" ],
          "lip-tint" => [ "value" => "Lip Tint", "img" => "assets/products/lip-tint.png" ],
          "compact-powder" => [ "value" => "Compact Powder", "img" => "assets/products/compact-powder-2.png" ],
          "loose-powder" => [ "value" => "Loose Powder", "img" => "assets/products/loose-powder.png" ],
          "cushion" => [ "value" => "Cushion", "img" => "assets/products/cushion.png" ],
          "cream-blush" => [ "value" => "Cream Blush", "img" => "assets/products/cream-blush.png" ],
          "pressed-blush" => [ "value" => "Pressed Blush", "img" => "assets/products/pressed-blush.png" ],
          "mascara" => [ "value" => "Mascara", "img" => "assets/products/mascara.png" ],
          "eyeliner" => [ "value" => "Eyeliner", "img" => "assets/products/eyeliner.png" ],
          "bb-cc-cream" => [ "value" => "BB/CC Cream", "img" => "assets/products/bb-cream.png" ],
          "sun-protection" => [ "value" => "Sunscreen/ Moisturizer", "img" => "assets/products/sun-protection.png" ],
          // "other" => [ "value" => "Other", "img" => "assets/lipstick.jpg" ]
        ]
      @endphp

      @foreach ($must_have as $key => $data)
        <div class="col-md-4 col-6 form-field--choices">
          <input type="checkbox" name="must-have[]" id="must-have-{{$key}}" value="{{$data['value']}}" class="form-field--radio"
            data-target="must-have-other-input"/>
          <label class="product__container" for="must-have-{{$key}}">
            <div class="choice-icon" style="text-align: center;">
              <img src="{{$data['img']}}" style="max-width:100%; height: 200px; padding: 24px 16px 12px;"/>
            </div>
            <p class="product__name" style="margin-bottom: 0;">
              {{$data['value']}}
            </p>
          </label>
        </div>
      @endforeach
      <div class="col-md-4 col-6 form-field--choices">
        <input type="checkbox" name="must-have[]" id="must-have-other" value="Other" class="form-field--radio"
          data-target="must-have-other-input"/>
        <label class="product__container" for="must-have-other">
          <div style="display: flex; width: 100%; align-items: center; justify-content: center; height: 200px;">
            <div class="choice-icon" style="text-align: center; padding: 24px 16px; width: 128px; height: 128px; vertical-align: middle;">
              @include('icons.plus-alt')
            </div>
          </div>
          <p class="product__name" style="margin-bottom: 0;">
            Other
          </p>
        </label>
      </div>

      <div style='width:100%; padding: 0 8px;'>
        <input name="must-have[]" id="must-have-other-input" placeholder="Please specify your must have products"
          autofocus="true" type='text' class="d-none form-field--text" disabled/>
      </div>

    </div>
  </div>

  @question([
    'step' => 'Step 3.1',
    'title' => 'What\' your skin tone?',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $skin_tones = [
          "light" => [ "value" => "Light", "icon" => "skin-tone-light" ],
          "medium-light" => [ "value" => "Medium Light", "icon" => "skin-tone-medium-light" ],
          "medium" => [ "value" => "Medium", "icon" => "skin-tone-medium" ],
          "medium-dark" => [ "value" => "Medium Dark", "icon" => "skin-tone-medium-dark" ],
          "dark" => [ "value" => "Dark", "icon" => "skin-tone-dark" ]
        ]
      @endphp

      @foreach ($skin_tones as $key => $data)
        <div class="col-md-3 col-sm-4 col-6 form-field--choices">
          <input type="radio" name="skin-tone" id="skin-tone-{{$key}}" value="{{$data['value']}}" class="form-field--radio" required/>
          <label class="choice-icon__container" for="skin-tone-{{$key}}">
            <div class="choice-icon icon--tone">
              @include("icons.".$data['icon'])
            </div>
            {{$data['value']}}
          </label>
        </div>
      @endforeach

    </div>
  </div>

  @question([
    'step' => 'Step 3.2',
    'title' => 'What\'s your skin undertone?',
    'subtitle' => 'Check your veins\' color on your wrist'
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $undertones = [
          "cool" => [ "value" => "Cool", "icon" => "undertone-cool" ],
          "neutral" => [ "value" => "Neutral", "icon" => "undertone-neutral" ],
          "warm" => [ "value" => "Warm", "icon" => "undertone-warm" ],
        ]
      @endphp

      @foreach ($undertones as $key => $data)
        <div class="col-md-4 col-sm-4 col-4 form-field--choices">
          <input type="radio" name="undertone" id="undertone-{{$key}}" value="{{$data['value']}}" class="form-field--radio" required/>
          <label class="choice-icon__container" for="undertone-{{$key}}">
            <div class="choice-icon">
              @include("icons.".$data['icon'])
            </div>
            {{$data['value']}}
          </label>
        </div>
      @endforeach

    </div>
  </div>

  @question([
    'step' => 'Step 4.1',
    'title' => 'How do you know about Emina?',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $medias = [
          "instagram" => [ "value" => "Instagram", "icon" => "source-ig" ],
          "friends" => [ "value" => "Friends", "icon" => "source-friend" ],
          "family" => [ "value" => "Family", "icon" => "source-family" ],
          "beauty-blogger" => [ "value" => "KOL / Beauty Blogger", "icon" => "source-blogger" ],
          "youtube" => [ "value" => "Youtube", "icon" => "source-youtube" ],
          "event" => [ "value" => "Event", "icon" => "source-event" ],
          "other" => [ "value" => "Other", "icon" => "source-other" ],
        ]
      @endphp

      @foreach ($medias as $key => $data)
        <div class="col-md-3 col-sm-4 col-6 form-field--choices">
          <input type="radio" name="how-you-know" id="how-you-know-{{$key}}" value="{{$data['value']}}" class="form-field--radio" required
            data-target="how-you-know-other-input"/>
          <label class="choice-icon__container" for="how-you-know-{{$key}}">
            <div class="choice-icon">
              @include("icons.".$data['icon'])
            </div>
            {{$data['value']}}
          </label>
        </div>
      @endforeach

      <div style='width:100%; padding: 0 8px;'>
        <input name="how-you-know-other" id="how-you-know-other-input" placeholder="Please specify how you know about emina"
          autofocus="true" type='text' class="d-none form-field--text" disabled/>
      </div>

    </div>
  </div>

  @question([
    'step' => 'Step 4.2',
    'title' => 'Which Emina products do you have?',
    'subtitle' => 'You can choose more than one option',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $products = [
          "creammatte" => [ "value" => "Creamatte Lip Cream", "img" => "assets/products/creammate.png" ],
          "sun-protection" => [ "value" => "Sunscreen/ Moisturizer", "img" => "assets/products/sun-protection.png" ],
          "cream-blush" => [ "value" => "Cheeklit Cream Blush", "img" => "assets/products/cream-blush.png" ],
          "pressed-blush" => [ "value" => "Cheeklit Pressed Blush", "img" => "assets/products/pressed-blush.png" ],
          "lip-tint" => [ "value" => "Magic Potion Lip Tint", "img" => "assets/products/lip-tint.png" ],
          "bright-stuff" => [ "value" => "Bright Stuff Moisturizing Cream", "img" => "assets/products/bright-stuff.png" ],
          // "other" => [ "value" => "Other", "img" => "assets/icons/plus-alt.svg" ],
        ]
      @endphp

      @foreach ($products as $key => $data)
        <div class="col-md-4 col-6 form-field--choices">
          <input type="checkbox" name="which-do-you-have[]" id="which-do-you-have-{{$key}}" value="{{$data['value']}}" class="form-field--radio"
            data-target="which-do-you-have-other-input"/>
          <label class="product__container" for="which-do-you-have-{{$key}}">
            <div class="choice-icon" style="text-align: center; padding-top: 8px;">
              <img src="{{$data['img']}}" style="max-width:100%; height: 200px; padding: 12px;"/>
            </div>
            <p class="product__name" style="margin-bottom: 0;">
              {{$data['value']}}
            </p>
          </label>
        </div>
      @endforeach
      <div class="col-md-4 col-6 form-field--choices">
        <input type="checkbox" name="which-do-you-have[]" id="which-do-you-have-other" value="Other" class="form-field--radio"
          data-target="which-do-you-have-other-input"/>
        <label class="product__container" for="which-do-you-have-other">
          <div style="display: flex; width: 100%; align-items: center; justify-content: center; height: 200px;">
            <div class="choice-icon" style="text-align: center; padding: 24px 16px; width: 128px; height: 128px; vertical-align: middle;">
              @include('icons.plus-alt')
            </div>
          </div>
          <p class="product__name" style="margin-bottom: 0;">
            Other
          </p>
        </label>
      </div>

      <div style="width:100%; padding: 0 8px;">
        <input name="which-do-you-have[]" id="which-do-you-have-other-input" placeholder="Please specify your products"
          autofocus="on" type='text' class="d-none form-field--text" disabled/>
      </div>

    </div>
  </div>

  @question([
    'step' => 'Step 5.2',
    'title' => 'What kind of product should Emina have for next launch?',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">
      <div class="col-sm-8 col-xs-12" style="margin: 0 auto">
        <textarea
          style="resize: vertical;"
          name="product-next-launch"
          id="product-next-launch"
          class="form-field--textarea"
          placeholder="Type your answer here..."
          rows="5"></textarea>
      </div>
    </div>
  </div>

  <div class="container-options">
    <div class="row">
      <div class="col-sm-8 col-xs-12" style="margin: 0 auto">
        <div class="row">
          <div class="col-sm-1">
            <input type="checkbox" id="privacy-policy" name="privacy-policy" value="true" checked>
          </div>
          <div class="col-sm-11">
            <p>I have read and agree the <a href="http://www.eminacosmetics.com/en/privacy-policy/" style="color:#E88593">privacy policy</a> of Emina</p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="row">
      @component('components.footer.footer', ['currentpage' => 1, 'submit' => true])
      @endcomponent
  </div>

</div>
