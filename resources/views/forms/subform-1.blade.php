@component('components.navbar.navbar-mobile')
@endcomponent

<div class="container-narrow">


  @question([
    'step' => 'Step 1',
    'title' => 'About me',
  ])
  @endquestion

  <div class="row">
    <div class="col-lg-6 col-sm-8 col-12" style="margin: 0 auto">

      <div class="form">
        <label for="name" class="labelSmall form__label">Nama <span style="color:#E88593;">*</span></label>
        <input type='text' name="name" id="name" class="form-field--text" placeholder="your name" value="{{ old('name') }}" required/>
      </div>

      <div class="form">
        <label for="dob" class="labelSmall form__label">Tanggal Lahir <span style="color:#E88593;">*</span></label>
        @if (session('error'))
            <label class="alert alert-danger" style="color:#E88593;">{{ session('error') }}</label>
        @endif
        <input type='text' name='dob' id="dob" class="form-field--text" placeholder="dd/mm/yyyy" value="{{ old('dob') }}" required/>
      </div>

      <div class="form">
        <label for="email" class="labelSmall form__label">Email <span style="color:#E88593;">*</span></label>
        <input type='text' name='email' id="email" class="form-field--text" placeholder="your email address" value="{{ old('email') }}" required/>
      </div>

      <div class="form">
        <label for="phone" class="labelSmall form__label">No. Telepon <span style="color:#E88593;">*</span></label>
        <input type='text' name='phone' id="phone" class="form-field--text" placeholder="your phone number" value="{{ old('phone') }}" required/>
      </div>

      <div class="form">
        <label for="ig-account" class="labelSmall form__label">Instagram Account <span class="form__label--optional">(Optional)</span></label>
        <div style="display: inline-flex; align-items: baseline; width: 100%;">
          <span style="padding: 12px 8px 12px 0; font-size: 17px;">@</span>
          <input type='text' name='ig-account' id="ig-account" class="form-field--text"
            placeholder="your instagram account" value="{{ old('ig-account') }}" />
        </div>
      </div>

      <div class="form">
        <?php
            $con = mysqli_connect("localhost","root","","emina_consumer");
        ?>
              <label id="domisili" class="labelSmall form__label">PROVINSI<span style="color:#E88593;">*</span></label>
             
                  <!--provinsi-->
                  <select id="provinsi" class="form-field--text" name="provinsi" required="">
                      <option value=""></option>
                      <?php
                          $query = mysqli_query($con, "SELECT * FROM provinsi ORDER BY provinsi");
                          while ($row = mysqli_fetch_array($query)) { ?>

                          <option value="<?php echo $row['provinsi']; ?>">
                              <?php echo $row['provinsi']; ?>
                          </option>

                      <?php } ?>
                  </select>
                  </div>
                  <div class="form">
                  <label id="domisili" class="labelSmall form__label">KABUPATEN/KOTA<span style="color:#E88593;">*</span></label>
              
                  <!--kota-->
                  <select id="kota" class="form-field--text" name="kota" required="">
                      <option value=""></option>
                      <?php
                          $query = mysqli_query($con, "SELECT * FROM kota INNER JOIN provinsi ON kota.id_provinsi_fk = provinsi.id_provinsi ORDER BY nama_kota");
                          while ($row = mysqli_fetch_array($query)) { ?>

                          <option id="kota" class="<?php echo $row['provinsi']; ?>" value="<?php echo $row['nama_kota']; ?>">
                              <?php echo $row['nama_kota']; ?>
                          </option>

                      <?php } ?>
                  </select>
             
             
              </div>
      
    
      
        
     

      <div class="form">
        <label for="money-spent" class="labelSmall form__label" style="margin-bottom: 16px;">Berapa pengeluaranmu untuk membeli kosmetik / produk kecantikan ? <span style="color:#E88593;">*</span></label>
        <label class="radio-container"> Kurang dari Rp 50.000/bulan
          <input type="radio" name="money-spent" class="form-field--radio" value='< 50.000'>
          <span class="radio-checkmark"></span>
        </label>
        <label class="radio-container">Rp 50.000 - Rp 100.000/bulan
          <input type="radio" name="money-spent" class="form-field--radio" value='50.000 - 100.000'>
          <span class="radio-checkmark"></span>
        </label>
        <label class="radio-container">Rp 100.000 - Rp 150.000/bulan
          <input type="radio" name="money-spent" class="form-field--radio" value='100.000 - 150.000'>
          <span class="radio-checkmark"></span>
        </label>
        <label class="radio-container">Rp 150.000 - Rp 200.000/bulan
          <input type="radio" name="money-spent" class="form-field--radio" value='150.000 - 200.000'>
          <span class="radio-checkmark"></span>
        </label>
        <label class="radio-container">Lebih dari Rp 200.000/bulan
          <input type="radio" name="money-spent" class="form-field--radio" value='> 200.000'>
          <span class="radio-checkmark"></span>
        </label>
      </div>

      <div class="form">
        <label for="what-you-do" class="labelSmall form__label" style="margin-bottom: 16px;">Apa pekerjaan kamu ? <span style="color:#E88593;">*</span></label>
        <label class="radio-container">Pelajar
          <input type="radio" name="what-you-do" class="form-field--radio" value='Student' data-target='what-you-do-other-input'>
          <span class="radio-checkmark"></span>
        </label>
        <label class="radio-container">Mahasiswa
          <input type="radio" name="what-you-do" class="form-field--radio" value='College' data-target='what-you-do-other-input'>
          <span class="radio-checkmark"></span>
        </label>
        <label class="radio-container">Karyawan
          <input type="radio" name="what-you-do" class="form-field--radio" value='Employee' data-target='what-you-do-other-input'>
          <span class="radio-checkmark"></span>
        </label>
        <label class="radio-container">Wirausaha
          <input type="radio" name="what-you-do" class="form-field--radio" value='Entrepreneur' data-target='what-you-do-other-input'>
          <span class="radio-checkmark"></span>
        </label>
        <label class="radio-container">Ibu Rumah Tangga
          <input type="radio" name="what-you-do" class="form-field--radio" value='Housewife' data-target='what-you-do-other-input'>
          <span class="radio-checkmark"></span>
        </label>
        <label class="radio-container">Lainnya
          <input type="radio" name="what-you-do" class="form-field--radio" value='Other' data-target='what-you-do-other-input'>
          <span class="radio-checkmark"></span>
        </label>
        <input
          disabled
          style="padding-top: 24px;"
          type='text'
          class="d-none form-field--text"
          name="what-you-do-other"
          autofocus="true"
          id="what-you-do-other-input"
          placeholder="Please specify what you do..." />
      </div>

    </div>
  </div>

  @question([
    'step' => 'Step 2.1',
    'title' => 'Apa tipe kulit kamu?',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $skin_types = [
          "normal" => [ "thumbnail"=>"normal", "value" => "Normal", "icon" => "skin-type-normal" ],
          "berminyak" => [ "thumbnail"=>"berminyak", "value" => "Oily", "icon" => "skin-type-oily" ],
          "kering" => [ "thumbnail"=>"kering", "value" => "Dry", "icon" => "skin-type-dry" ],
          "kombinasi" => [ "thumbnail"=>"kombinasi", "value" => "Combination", "icon" => "skin-type-combined" ]
        ]
      @endphp

      @foreach ($skin_types as $key => $data)
        <div class="col-md-3 col-sm-4 col-6 form-field--choices">
          <input type="radio" name="skin-type" id="skin-type-{{$key}}" value="{{$data['value']}}" class="form-field--radio" required/>
          <label class="choice-icon__container" for="skin-type-{{$key}}">
            <div class="choice-icon">
              @include("icons.".$data['icon'])
            </div>
            {{$data['thumbnail']}}
          </label>
        </div>
      @endforeach

    </div>
  </div>

  @question([
    'step' => 'Step 2.2',
    'title' => 'Apa masalah kulit kamu?',
    'subtitle' => 'You can choose more than one option'
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $skin_concerns = [
          "jerawat" => [ "thumbnail"=> "jerawat", "value" => "Acne", "icon" => "skin-concern-acne" ],
          "bekas jerawat" => [ "thumbnail"=> "bekas jerawat", "value" => "Acne Scars", "icon" => "skin-concern-acne-scars" ],
          "pori besar" => [ "thumbnail"=> "pori besar", "value" => "Large Pores", "icon" => "skin-concern-large-pores" ],
          "kulit kusam" => [ "thumbnail"=> "kulit kusam", "value" => "Dull Skin", "icon" => "skin-concern-dull" ],
          "kulit sensitif" => [ "thumbnail"=> "kulit sensitif", "value" => "Sensitive", "icon" => "skin-concern-sensitive" ],
          "warna kulit tidak merata" => [ "thumbnail"=> "warna kulit tidak merata", "value" => "Uneven Skin Tone", "icon" => "skin-concern-uneven" ],
          "komedo" => [ "thumbnail"=> "komedo", "value" => "Black/White Head", "icon" => "skin-concern-blackhead" ],
          "lainnya" => [ "thumbnail"=> "lainnya", "value" => "Other", "icon" => "plus" ]
        ]
      @endphp

      @foreach ($skin_concerns as $key => $data)
        <div class="col-md-3 col-sm-4 col-6 form-field--choices">
          <input type="checkbox" name="skin-concern[]" id="skin-concern-{{$key}}" value="{{$data['value']}}" class="form-field--radio"
            data-target="skin-concern-other-input"/>
          <label class="choice-icon__container" for="skin-concern-{{$key}}">
            <div class="choice-icon">
              @include("icons.".$data['icon'])
            </div>
            {{$data['thumbnail']}}
          </label>
        </div>
      @endforeach

      <div class="col-sm-12">
          <input
            type='text'
            style="padding: 16px 0;"
            id="skin-concern-other-input"
            class="d-none form-field--text"
            name="skin-concern[]"
            autofocus="true"
            placeholder="Please specify your skin concern..."
            disabled />
      </div>

    </div>
  </div>



  @question([
    'step' => 'Step 3.1',
    'title' => 'Apa warna kulit kamu?',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $skin_tones = [
          "light" => [ "value" => "Light", "icon" => "skin-tone-light" ],
          "medium-light" => [ "value" => "Medium Light", "icon" => "skin-tone-medium-light" ],
          "medium" => [ "value" => "Medium", "icon" => "skin-tone-medium" ],
          "medium-dark" => [ "value" => "Medium Dark", "icon" => "skin-tone-medium-dark" ],
          "dark" => [ "value" => "Dark", "icon" => "skin-tone-dark" ]
        ]
      @endphp

      @foreach ($skin_tones as $key => $data)
        <div class="col-md-3 col-sm-4 col-6 form-field--choices">
          <input type="radio" name="skin-tone" id="skin-tone-{{$key}}" value="{{$data['value']}}" class="form-field--radio" required/>
          <label class="choice-icon__container" for="skin-tone-{{$key}}">
            <div class="choice-icon icon--tone">
              @include("icons.".$data['icon'])
            </div>
            {{$data['value']}}
          </label>
        </div>
      @endforeach

    </div>
  </div>

  @question([
    'step' => 'Step 3.2',
    'title' => 'Kalau warna dasar kulitmu?',
    'subtitle' => 'Untuk cek warna dasar bisa dilihat dari warna urat pada pergelangan tanganmu'
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $undertones = [
          "cool" => [ "value" => "Cool", "icon" => "undertone-cool" ],
          "neutral" => [ "value" => "Neutral", "icon" => "undertone-neutral" ],
          "warm" => [ "value" => "Warm", "icon" => "undertone-warm" ],
        ]
      @endphp

      @foreach ($undertones as $key => $data)
        <div class="col-md-4 col-sm-4 col-4 form-field--choices">
          <input type="radio" name="undertone" id="undertone-{{$key}}" value="{{$data['value']}}" class="form-field--radio" required/>
          <label class="choice-icon__container" for="undertone-{{$key}}">
            <div class="choice-icon">
              @include("icons.".$data['icon'])
            </div>
            {{$data['value']}}
          </label>
        </div>
      @endforeach

    </div>
  </div>

  @question([
    'step' => 'Step 4.1',
    'title' => 'Darimana kamu mengetahui Emina?',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $medias = [
          "instagram" => [ "thumbnail"=>"media sosial", "value" => "media sosial", "icon" => "source-ig" ],
          "friends" => [ "thumbnail"=>"teman", "value" => "Friends", "icon" => "source-friend" ],
          "family" => [ "thumbnail"=>"keluarga", "value" => "Family", "icon" => "source-family" ],
          "beauty-blogger" => [ "thumbnail"=>"televisi", "value" => "televisi", "icon" => "source-blogger" ],
          "youtuber" => [ "thumbnail"=>"youtuber", "value" => "Youtube", "icon" => "source-youtube" ],
          "event" => [ "thumbnail"=>"acara", "value" => "Event", "icon" => "source-event" ],
          "other" => [ "thumbnail"=>"lainnya", "value" => "Other", "icon" => "source-other" ],
        ]
      @endphp

      @foreach ($medias as $key => $data)
        <div class="col-md-3 col-sm-4 col-6 form-field--choices">
          <input type="radio" name="how-you-know" id="how-you-know-{{$key}}" value="{{$data['value']}}" class="form-field--radio" required
            data-target="how-you-know-other-input"/>
          <label class="choice-icon__container" for="how-you-know-{{$key}}">
            <div class="choice-icon">
              @include("icons.".$data['icon'])
            </div>
            {{$data['thumbnail']}}
          </label>
        </div>
      @endforeach

      <div style='width:100%; padding: 0 8px;'>
        <input name="how-you-know-other" id="how-you-know-other-input" placeholder="Please specify how you know about emina"
          autofocus="true" type='text' class="d-none form-field--text" disabled/>
      </div>

    </div>
  </div>

  @question([
    'step' => 'Step 4.2',
    'title' => 'Produk Emina apa yang kamu miliki?',
    'subtitle' => 'Kamu bisa milih lebih dari satu',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $products = [
          "creammatte" => [ "value" => "Creamatte Lip Cream", "img" => "assets/products/creammate.png" ],
          "sun-protection" => [ "value" => "Sunscreen/ Moisturizer", "img" => "assets/products/sun-protection.png" ],
          "cream-blush" => [ "value" => "Cheeklit Cream Blush", "img" => "assets/products/cream-blush.png" ],
          "pressed-blush" => [ "value" => "Cheeklit Pressed Blush", "img" => "assets/products/pressed-blush.png" ],
          "lip-tint" => [ "value" => "Magic Potion Lip Tint", "img" => "assets/products/lip-tint.png" ],
          "bright-stuff" => [ "value" => "Bright Stuff Moisturizing Cream", "img" => "assets/products/bright-stuff.png" ],
          // "other" => [ "value" => "Other", "img" => "assets/icons/plus-alt.svg" ],
        ]
      @endphp

      @foreach ($products as $key => $data)
        <div class="col-md-4 col-6 form-field--choices">
          <input type="checkbox" name="which-do-you-have[]" id="which-do-you-have-{{$key}}" value="{{$data['value']}}" class="form-field--radio"
            data-target="which-do-you-have-other-input"/>
          <label class="product__container" for="which-do-you-have-{{$key}}">
            <div class="choice-icon" style="text-align: center; padding-top: 8px;">
              <img src="{{$data['img']}}" style="max-width:100%; height: 200px; padding: 12px;"/>
            </div>
            <p class="product__name" style="margin-bottom: 0;">
              {{$data['value']}}
            </p>
          </label>
        </div>
      @endforeach
      <div class="col-md-4 col-6 form-field--choices">
        <input type="checkbox" name="which-do-you-have[]" id="which-do-you-have-other" value="Other" class="form-field--radio"
          data-target="which-do-you-have-other-input"/>
        <label class="product__container" for="which-do-you-have-other">
          <div style="display: flex; width: 100%; align-items: center; justify-content: center; height: 200px;">
            <div class="choice-icon" style="text-align: center; padding: 24px 16px; width: 128px; height: 128px; vertical-align: middle;">
              @include('icons.plus-alt')
            </div>
          </div>
          <p class="product__name" style="margin-bottom: 0;">
            Other
          </p>
        </label>
      </div>

      <div style="width:100%; padding: 0 8px;">
        <input name="which-do-you-have[]" id="which-do-you-have-other-input" placeholder="Please specify your products"
          autofocus="on" type='text' class="d-none form-field--text" disabled/>
      </div>

    </div>
  </div>

  @question([
    'step' => 'Step 4.3',
    'title' => 'Produk kosmetik apa yang harus kamu selalu bawa?',
    'subtitle' => 'Kamu bisa milih lebih dari satu',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $must_have = [
          "lip-cream" => [ "value" => "Lip Cream", "img" => "assets/products/creammate.png" ],
          "lip-tint" => [ "value" => "Lip Tint", "img" => "assets/products/lip-tint.png" ],
          "compact-powder" => [ "value" => "Compact Powder", "img" => "assets/products/compact-powder-2.png" ],
          "loose-powder" => [ "value" => "Loose Powder", "img" => "assets/products/loose-powder.png" ],
          "cushion" => [ "value" => "Cushion", "img" => "assets/products/cushion.png" ],
          "cream-blush" => [ "value" => "Cream Blush", "img" => "assets/products/cream-blush.png" ],
          "pressed-blush" => [ "value" => "Pressed Blush", "img" => "assets/products/pressed-blush.png" ],
          "mascara" => [ "value" => "Mascara", "img" => "assets/products/mascara.png" ],
          "eyeliner" => [ "value" => "Eyeliner", "img" => "assets/products/eyeliner.png" ],
          "bb-cc-cream" => [ "value" => "BB/CC Cream", "img" => "assets/products/bb-cream.png" ],
          "sun-protection" => [ "value" => "Sunscreen/ Moisturizer", "img" => "assets/products/sun-protection.png" ],
          // "other" => [ "value" => "Other", "img" => "assets/lipstick.jpg" ]
        ]
      @endphp

      @foreach ($must_have as $key => $data)
        <div class="col-md-4 col-6 form-field--choices">
          <input type="checkbox" name="must-have[]" id="must-have-{{$key}}" value="{{$data['value']}}" class="form-field--radio"
            data-target="must-have-other-input"/>
          <label class="product__container" for="must-have-{{$key}}">
            <div class="choice-icon" style="text-align: center;">
              <img src="{{$data['img']}}" style="max-width:100%; height: 200px; padding: 24px 16px 12px;"/>
            </div>
            <p class="product__name" style="margin-bottom: 0;">
              {{$data['value']}}
            </p>
          </label>
        </div>
      @endforeach
      <div class="col-md-4 col-6 form-field--choices">
        <input type="checkbox" name="must-have[]" id="must-have-other" value="Other" class="form-field--radio"
          data-target="must-have-other-input"/>
        <label class="product__container" for="must-have-other">
          <div style="display: flex; width: 100%; align-items: center; justify-content: center; height: 200px;">
            <div class="choice-icon" style="text-align: center; padding: 24px 16px; width: 128px; height: 128px; vertical-align: middle;">
              @include('icons.plus-alt')
            </div>
          </div>
          <p class="product__name" style="margin-bottom: 0;">
            Other
          </p>
        </label>
      </div>

      <div style='width:100%; padding: 0 8px;'>
        <input name="must-have[]" id="must-have-other-input" placeholder="Please specify your must have products"
          autofocus="true" type='text' class="d-none form-field--text" disabled/>
      </div>

    </div>
  </div>


  <div class="row">
    @component('components.footer.footer', ['currentpage' => 0])
    @endcomponent
  </div>

</div>
