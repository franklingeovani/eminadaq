@component('components.navbar.navbar-mobile')
@endcomponent

<div class="container-narrow" style="padding-top: 48px; padding-bottom: 48px;">

  @question([
    'step' => 'Step 4.1',
    'title' => 'How do you know about Emina?',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $medias = [
          "instagram" => [ "value" => "Instagram", "icon" => "source-ig" ],
          "friends" => [ "value" => "Friends", "icon" => "source-friend" ],
          "family" => [ "value" => "Family", "icon" => "source-family" ],
          "beauty-blogger" => [ "value" => "KOL / Beauty Blogger", "icon" => "source-blogger" ],
          "youtube" => [ "value" => "Youtube", "icon" => "source-youtube" ],
          "event" => [ "value" => "Event", "icon" => "source-event" ],
          "other" => [ "value" => "Other", "icon" => "source-other" ],
        ]
      @endphp

      @foreach ($medias as $key => $data)
        <div class="col-md-3 col-sm-4 col-6 form-field--choices">
          <input type="radio" name="how-you-know" id="how-you-know-{{$key}}" value="{{$data['value']}}" class="form-field--radio" required
            data-target="how-you-know-other-input"/>
          <label class="choice-icon__container" for="how-you-know-{{$key}}">
            <div class="choice-icon">
              @include("icons.".$data['icon'])
            </div>
            {{$data['value']}}
          </label>
        </div>
      @endforeach

      <div style='width:100%; padding: 0 8px;'>
        <input name="how-you-know-other" id="how-you-know-other-input" placeholder="Please specify how you know about emina"
          autofocus="true" type='text' class="d-none form-field--text" disabled/>
      </div>

    </div>
  </div>

  @question([
    'step' => 'Step 4.2',
    'title' => 'Which Emina products do you have?',
    'subtitle' => 'You can choose more than one option',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $products = [
          "creammatte" => [ "value" => "Creamatte Lip Cream", "img" => "assets/products/creammate.png" ],
          "sun-protection" => [ "value" => "Sunscreen/ Moisturizer", "img" => "assets/products/sun-protection.png" ],
          "cream-blush" => [ "value" => "Cheeklit Cream Blush", "img" => "assets/products/cream-blush.png" ],
          "pressed-blush" => [ "value" => "Cheeklit Pressed Blush", "img" => "assets/products/pressed-blush.png" ],
          "lip-tint" => [ "value" => "Magic Potion Lip Tint", "img" => "assets/products/lip-tint.png" ],
          "bright-stuff" => [ "value" => "Bright Stuff Moisturizing Cream", "img" => "assets/products/bright-stuff.png" ],
          // "other" => [ "value" => "Other", "img" => "assets/icons/plus-alt.svg" ],
        ]
      @endphp

      @foreach ($products as $key => $data)
        <div class="col-md-4 col-6 form-field--choices">
          <input type="checkbox" name="which-do-you-have[]" id="which-do-you-have-{{$key}}" value="{{$data['value']}}" class="form-field--radio"
            data-target="which-do-you-have-other-input"/>
          <label class="product__container" for="which-do-you-have-{{$key}}">
            <div class="choice-icon" style="text-align: center; padding-top: 8px;">
              <img src="{{$data['img']}}" style="max-width:100%; height: 200px; padding: 12px;"/>
            </div>
            <p class="product__name" style="margin-bottom: 0;">
              {{$data['value']}}
            </p>
          </label>
        </div>
      @endforeach
      <div class="col-md-4 col-6 form-field--choices">
        <input type="checkbox" name="which-do-you-have[]" id="which-do-you-have-other" value="Other" class="form-field--radio"
          data-target="which-do-you-have-other-input"/>
        <label class="product__container" for="which-do-you-have-other">
          <div style="display: flex; width: 100%; align-items: center; justify-content: center; height: 200px;">
            <div class="choice-icon" style="text-align: center; padding: 24px 16px; width: 128px; height: 128px; vertical-align: middle;">
              @include('icons.plus-alt')
            </div>
          </div>
          <p class="product__name" style="margin-bottom: 0;">
            Other
          </p>
        </label>
      </div>

      <div style="width:100%; padding: 0 8px;">
        <input name="which-do-you-have[]" id="which-do-you-have-other-input" placeholder="Please specify your products"
          autofocus="on" type='text' class="d-none form-field--text" disabled/>
      </div>

    </div>
  </div>

  <div class="row">
    @component('components.footer.footer', ['currentpage' => 3])
    @endcomponent
  </div>

</div>
