@component('components.navbar.navbar-mobile')
@endcomponent

<div class="container-narrow">

  @question([
    'step' => 'Step 3.1',
    'title' => 'What\' your skin tone?',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $skin_tones = [
          "light" => [ "value" => "Light", "icon" => "skin-tone-light" ],
          "medium-light" => [ "value" => "Medium Light", "icon" => "skin-tone-medium-light" ],
          "medium" => [ "value" => "Medium", "icon" => "skin-tone-medium" ],
          "medium-dark" => [ "value" => "Medium Dark", "icon" => "skin-tone-medium-dark" ],
          "dark" => [ "value" => "Dark", "icon" => "skin-tone-dark" ]
        ]
      @endphp

      @foreach ($skin_tones as $key => $data)
        <div class="col-md-3 col-sm-4 col-6 form-field--choices">
          <input type="radio" name="skin-tone" id="skin-tone-{{$key}}" value="{{$data['value']}}" class="form-field--radio" required/>
          <label class="choice-icon__container" for="skin-tone-{{$key}}">
            <div class="choice-icon icon--tone">
              @include("icons.".$data['icon'])
            </div>
            {{$data['value']}}
          </label>
        </div>
      @endforeach

    </div>
  </div>

  @question([
    'step' => 'Step 3.2',
    'title' => 'What\'s your skin undertone?',
    'subtitle' => 'Check your veins\' color on your wrist'
  ])
  @endquestion

  <div class="container-options">
    <div class="row">

      @php
        $undertones = [
          "cool" => [ "value" => "Cool", "icon" => "undertone-cool" ],
          "neutral" => [ "value" => "Neutral", "icon" => "undertone-neutral" ],
          "warm" => [ "value" => "Warm", "icon" => "undertone-warm" ],
        ]
      @endphp

      @foreach ($undertones as $key => $data)
        <div class="col-md-4 col-sm-4 col-4 form-field--choices">
          <input type="radio" name="undertone" id="undertone-{{$key}}" value="{{$data['value']}}" class="form-field--radio" required/>
          <label class="choice-icon__container" for="undertone-{{$key}}">
            <div class="choice-icon">
              @include("icons.".$data['icon'])
            </div>
            {{$data['value']}}
          </label>
        </div>
      @endforeach

    </div>
  </div>

  <div class="row">
    @component('components.footer.footer', ['currentpage' => 2])
    @endcomponent
  </div>

  </div>
