@component('components.navbar.navbar-mobile')
@endcomponent

<div class="container-narrow">
@question([
    'step' => 'Step 5.1',
    'title' => 'Produk kecantikan (make up/skin care) apa yang paling banyak orang bicarakan saat ini?',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">
      <div class="col-sm-8 col-xs-12" style="margin: 0 auto">
        <textarea
          style="resize: vertical;"
          name="happening-product"
          id="happening-product"
          class="form-field--textarea"
          placeholder="Type your answer here..."
          rows="5" required></textarea>
      </div>
    </div>
  </div>

  @question([
    'step' => 'Step 5.2',
    'title' => 'Produk apa menurutmu yang harus Emina keluarkan selanjutnya?',
  ])
  @endquestion

  <div class="container-options">
    <div class="row">
      <div class="col-sm-8 col-xs-12" style="margin: 0 auto">
        <textarea
          style="resize: vertical;"
          name="product-next-launch"
          id="product-next-launch"
          class="form-field--textarea"
          placeholder="Type your answer here..."
          rows="5"></textarea>
      </div>
    </div>
  </div>


  <div class="container-options">
    <div class="row">
      <div class="col-sm-8 col-xs-12" style="margin: 0 auto">
        <div class="row">
          <div class="col-sm-1">
            <input type="checkbox" id="privacy-policy" name="privacy-policy" checked>
             <input type="checkbox" id="privacy-policy" name="privacy-policy" checked>
          </div>
          <div class="col-sm-11">
            <p>I have read and agree the <a href="http://www.eminacosmetics.com/en/privacy-policy/" style="color:#E88593">privacy policy</a> of Emina</p>
            <p>Saya setuju menerima email berupa informasi terbaru terkait brand</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    @component('components.footer.footer', ['currentpage' => 1,'submit'=>true])
    @endcomponent
  </div>
</div>
