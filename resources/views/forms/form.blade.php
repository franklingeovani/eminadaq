@extends('layouts.default')

@push('component-styles')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css" />
@endpush

@section('content')
  @component('components.navbar.navbar')
  @endcomponent
  <form autocomplete="false" method="post" action="/consumer" id="survey-form" class="pt-perspective">
    {{ csrf_field() }}
    <div class="pt-page pt-page-1">
      @component('forms.subform-1')
      @endcomponent
      @include('components.stickers.stickers-mobile')
    </div>
     <div class="pt-page pt-page-2">
      @component('forms.subform-2')
      @endcomponent
      @include('components.stickers.stickers-mobile')
    </div>
    <!--  <div class="pt-page pt-page-3">
      @component('forms.subform-3')
      @endcomponent
      @include('components.stickers.stickers-mobile')
    </div>
    <div class="pt-page pt-page-4">
      @component('forms.subform-4')
      @endcomponent
      @include('components.stickers.stickers-mobile')
    </div>
    <div class="pt-page pt-page-5">
      @component('forms.subform-5')
      @endcomponent
      @include('components.stickers.stickers-mobile')
    </div> -->
  </form>
  @include('components.stickers.stickers', ['form' => true])
@endsection

@push('component-scripts')
  <script
    src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js">
  </script>
  <script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous">
  </script>
  <script
    src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js">
  </script>
  <script src="{{ mix('/js/survey-form.js') }}"></script>
  
  <script src="{{'/js/jquery-chained.min.js'}}"></script>
         
      
        
@endpush
