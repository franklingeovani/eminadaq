<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumerSkinProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumer_skin_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->required();
            $table->string('skin-type');
            $table->string('skin-concern');
            $table->string('skin-tone');
            $table->string('undertone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumer_skin_profiles');
    }
}
