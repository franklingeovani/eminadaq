<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumerInstagramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumer_instagrams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('account')->nullable();
            $table->integer('follower')->nullable();
            $table->integer('following')->nullable();
            $table->integer('posts')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumer_instagrams');
    }
}
