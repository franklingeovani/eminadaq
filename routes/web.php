<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// --------------------------------------------------------------------------
// FOR LIVE
// --------------------------------------------------------------------------

Route::resource('/consumer', 'ConsumerDataController');

Route::get('/instagram', 'DashboardController@getInstagram');
Route::get('/draftemail', 'Email@previewEmail');
Route::get('/sendemail', 'Email@sendEmail')->name('sendingEmail');
Route::get('/testemail', 'Email@testEmail');
Route::get('/timezone', 'DashboardController@checkTimezone');
Route::get('/verify/email/{parameter}', 'ConsumerDataController@verification');
Route::get('/dashboard', 'DashboardController@Dashboard');
Route::get('/dashboard/emina/search', 'DashboardController@searchEmina');
Route::get('/home/{medium}/{source}/{campaign}', 'SourceController@tracking');


Route::get('/', function () {
    return view('home');
});

Route::get('/form', function () {
    return view('forms/form');
});

Route::post('/form', function(Request $request) {
    // dd($request->all());
    return view('voucher-alt');
});

Route::get('/voucher', function() {
    // dd($request->all());
    return view('voucher');
});

Route::get('/voucher-subscribed', function() {
    // dd($request->all());
    return view('voucher-subscribed');
});

Route::get('/voucher-alt', function() {
  // dd($request->all());
  return view('voucher-alt');
});

Route::match(['get','post'], '/login', 'LoginController@login');
Route::match(['get','post'], '/loginview', 'LoginController@view')->name('login');

Route::group(['middleware' => 'auth'], function() {
});
View::composer(['*'], function($view){
  $user = Auth::user();
  $view->with('user', $user);
});

// --------------------------------------------------------------------------
// END OF LIVE ROUTES
// --------------------------------------------------------------------------


// --------------------------------------------------------------------------
// FOR DEVELOPMENT
// --------------------------------------------------------------------------


Route::get('/testutm', 'ConsumerDataController@testUtm');
// Route::get('/register', function() {
//     return view('auth.register');
// });
// Route::get('/app', function() {
//     return view('layouts.app');
// });
Route::get('/getdata', 'ConsumerDataController@getData');



// --------------------------------------------------------------------------
// END OF DEV ROUTES
// --------------------------------------------------------------------------
