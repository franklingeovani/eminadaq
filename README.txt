Migration From Local to Server.

1. Check if all the source is access from with safe connection (SSL) by using secure_asset
2. Check the .env file that the application connection and configuration is working, especially the database connection
3. Check all the URL generator and all the routes is working
4. Check the email.blade.php to ensure that the content of the email is right and the link is working.
5. Check the mail.php in config directory to change the email configuration
6. Check the .env file to check the email parameter is working if there is an alteration.
7. Try to check all the possibilities input
