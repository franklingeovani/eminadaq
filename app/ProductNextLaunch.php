<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductNextLaunch extends Model
{
  protected $table = 'product_next_launches';

  protected $fillable =[
    'email',
    'product-next-launch',
    'happening-product',
  ];
}
