<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consumer;
use App\ConsumerInstagram;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;



class DashboardController extends Controller
{
    public function Dashboard()
    {
      Session::flush();

      $contacts = Consumer::simplePaginate(10);
      $consumer_contacts = Consumer::all();
      $recent_contact = Consumer::whereDate('created_at', Carbon::today())->get();

      $count_recent_contact = $recent_contact->count();
      $count_contact = $consumer_contacts->count();
      $count_verified_contact = Consumer::where('is_verified', TRUE)->count();
      $count_unverified_contact = $count_contact - $count_verified_contact;
      $count_occupation_school = Consumer::where('what-you-do', 'School')->count();
      $count_occupation_college = Consumer::where('what-you-do', 'College')->count();
      $count_occupation_work = Consumer::where('what-you-do', 'Work')->count();

      return view('dashboard', compact('contacts'))->with('count_recent_contact', $count_recent_contact)
                                                   ->with('count_contact', $count_contact)
                                                   ->with('count_verified_contact', $count_verified_contact)
                                                   ->with('count_unverified_contact', $count_unverified_contact)
                                                   ->with('count_occupation_school', $count_occupation_school)
                                                   ->with('count_occupation_college', $count_occupation_college)
                                                   ->with('count_occupation_work', $count_occupation_work);
    }

    public function searchEmina(Request $request)
      {
        $brand = "Emina";
        $contacts = Consumer::where('name', 'LIKE', '%'.$request->input('search').'%')
                              ->orWhere('email', 'LIKE', '%'.$request->input('search').'%')
                              ->orWhere('phone', 'LIKE', '%'.$request->input('search').'%')
                              ->simplePaginate(10);


        Session::flash('search', TRUE);
        $consumer_contacts = Consumer::all();
        $recent_contact = Consumer::whereDate('created_at', Carbon::today())->get();

        $count_recent_contact = $recent_contact->count();
        $count_contact = $consumer_contacts->count();
        $count_verified_contact = Consumer::where('is_verified', TRUE)->count();
        $count_unverified_contact = $count_contact - $count_verified_contact;
        $count_occupation_school = Consumer::where('what-you-do', 'School')->count();
        $count_occupation_college = Consumer::where('what-you-do', 'College')->count();
        $count_occupation_work = Consumer::where('what-you-do', 'Work')->count();


        return view('dashboard', compact('contacts'))->with('count_recent_contact', $count_recent_contact)
                                                     ->with('count_contact', $count_contact)
                                                     ->with('count_verified_contact', $count_verified_contact)
                                                     ->with('count_unverified_contact', $count_unverified_contact)
                                                     ->with('count_occupation_school', $count_occupation_school)
                                                     ->with('count_occupation_college', $count_occupation_college)
                                                     ->with('count_occupation_work', $count_occupation_work)
                                                     ->with('brand', $brand);
      }

    public function checkTimezone()
    {
        $time = Carbon::now()->toDateTimeString();;
        echo ($time);
    }



  }
