<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class SourceController extends Controller
{
  public function tracking($medium, $source, $campaign){
      Session::put('source_data', ['medium'=>$medium, 'source'=>$source, 'campaign'=>$campaign]);
      return view('home');

  }

}
