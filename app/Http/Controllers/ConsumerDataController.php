<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consumer;
use App\ConsumerSkinProfile;
use App\EminaProductHave;
use App\MustHaveProduct;
use App\ProductNextLaunch;
use App\SourceInformation;
use App\ConsumerInstagram;
use App\DataSourceTracking;
use App\Libraries\ReferralGrabber;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Session;

class ConsumerDataController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $source_data = Session::get('source_data');
            DataSourceTracking::create(['email' => $request->email,
                                      'medium' => $source_data['medium'],
                                      'source' => $source_data['source'],
                                      'campaign' => $source_data['campaign'],
                                    ]);

            $temp = Consumer::where('email', $request->email)->first();
            if (!empty($temp)){
              $is_verified = $temp->is_verified;
              $consumerdata = $temp;
              if($is_verified == 0){
                $request->session()->put('consumerdata', $consumerdata);
                return redirect ('/sendemail');
              } else {
                return view('voucher-subscribed');
              }
            } else {
              $date = $request->dob;
              $dob = Carbon::createFromFormat('d/m/Y',$date);

              Consumer::create(['name' => $request->name,
                                'dob' => $dob,
                                'email' => $request->email,
                                'phone' => $request->phone,
                                'ig-account' => $request->{'ig-account'},
                                'provinsi' => $request->{'provinsi'},
                                'kota' => $request->{'kota'},
                                'what-you-do' => $request->{'what-you-do'},
                                'money-spent' => $request->{'money-spent'}]);

              $skin_concern = implode(",",$_POST["skin-concern"]);
              $emina_product_have = implode(",",$_POST["which-do-you-have"]);
              $must_have_product = implode(",",$_POST["must-have"]);


              ConsumerSkinProfile::create(['email' => $request->email,
                                            'skin-type' => $request->{'skin-type'},
                                            'skin-concern' => $skin_concern,
                                            'skin-tone' => $request->{'skin-tone'},
                                            'undertone' => $request->undertone
                                          ]);

              EminaProductHave::create(['email' => $request->email,
                                        'which-do-you-have' => $emina_product_have,
                                      ]);

              MustHaveProduct::create(['email' => $request->email,
                                        'must-have' => $must_have_product,
                                      ]);
              if (!empty($request->{'product-next-launch'})){
                ProductNextLaunch::create(['email' => $request->email,
                                          'product-next-launch' => $request->{'product-next-launch'},
                                          'happening-product' => $request->{'happening-product'},
                                        ]);
              }

              SourceInformation::create(['email' => $request->email,
                                        'how-you-know' => $request->{'how-you-know'},
                                      ]);


              // if(!empty($request->{'ig-account'})){
              //     $get_instagram = $this->getInstagram('https://instagram.com/'.$request->{'ig-account'});
              //     // dd($get_instagram);
              //     if(!empty($get_instagram)){
              //       $follower = str_replace(",", "", $get_instagram[0]);
              //       $following = str_replace(",", "", $get_instagram[1]);
              //       ConsumerInstagram::create(['account' => $request->{'ig-account'},
              //                                   'follower' => $follower,
              //                                   'following' => $following,
              //                                   'posts' => $get_instagram[2]]
              //                                 );
              //     }
              // }

              $receiver = $request->name;
              $sendto = $request->email;
              $consumerdata = ['name'=>$receiver, 'email'=>$sendto];
              $request->session()->put('consumerdata', $consumerdata);

              // dd($get_instagram);

              return redirect('/sendemail');
              // return view('voucher-alt');
            }
        } catch(\InvalidArgumentException $e) {
          return back()->with('error','Please insert the Date Of Birth With DD/MM/YYYY Format')->withInput($request->all);
        } catch(\Illuminate\Database\QuerryException $e){
          echo("Oops.... There must be something wrong with the connection");
        }
        // dd($request->all());\

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function testUtm()
    {
      if (empty($_COOKIE['notice']) || $_COOKIE['notice'] != 'yes') {
        // error
        echo 'Cookie does not exists, is empty or does not equal "yes".';
      } else {
        $data = ReferralGrabber::parseGoogleCookie($_COOKIE['__utmz']);
        dd($data);
      }

    }

    public function verification($parameter)
    {
      $email = Crypt::decrypt($parameter);

      $get_consumer = Consumer::where('email', $email)->first();
      $is_verified = $get_consumer->is_verified;
      if ($is_verified == False){
        $get_consumer->is_verified = True;
        $get_consumer->save();
        return view('voucher-verified');
      } else {
        return view('voucher-subscribed');
      }
      // echo ($email);
    }


    public function getData()
    {
        $consumer = consumer::with('get_skin_profile')->first();
        echo ($consumer->get_skin_profile['skin-type']);
    }

    public function getInstagram($pageUrl) {
            $url = $pageUrl;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_REFERER, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $result = curl_exec($ch);
            curl_close($ch);

            $output;
            $metaPos = strpos($result, "<meta content=");
            if($metaPos != false)
            {
                $meta = substr($result ,$metaPos,70);

                //meghdare followers
                $followerPos = strpos($meta , "Followers");
                $followers = substr($meta , 15 , $followerPos-15);
                $output[0] = $followers;

                //meghdare followings
                // $commaPos = strpos($meta , ',');
                $followingPos = strpos($meta, 'Following');
                $followings = substr($meta , $followerPos+10 , $followingPos - ($followerPos+10));
                $output[1] = $followings;


                //meghdare posts
                $seccondCommaPos = $followingPos + 10;
                $postsPos = strpos($meta, 'Post');
                $posts = substr($meta, $seccondCommaPos , $postsPos - $seccondCommaPos);
                $output[2] = $posts;

                  //image finder
                    // $imgPos = strpos($result, "og:image");
                    // $image = substr($result , $imgPos+19 , 200);
                    // $endimgPos = strpos($image, "/>");
                    // $finalImagePos = substr($result, $imgPos+19 , $endimgPos-2);
                    // $output[3] = $finalImagePos;

                return $output;
            }
            else
            {
                return null;
            }
        }

}
