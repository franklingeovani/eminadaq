<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Mail; //Jangan lupa import Mail di paling atas

class Email extends Controller
{
    public function previewEmail()
    {
      $nama = "Belva Prima A";
      $pesan = "This is the message";
      return view('email')->with('nama', $nama)->with('pesan', $pesan);
    }


    public function sendEmail(Request $request)
    {
      $consumerdata = $request->session()->get('consumerdata');
      // dd($consumerdata['email']);
      $parameter = Crypt::encrypt($consumerdata['email']);
      try{
          // $pesan = "eminadaq.local/".$

          // Mail::send('email', ['nama' => $request->name, 'pesan' => $request->pesan], function ($message) use ($request)
          Mail::send('email', ['nama' => $consumerdata['name'], 'pesan' => $parameter], function ($message) use ($consumerdata)
          {
              $message->subject('Emina Cosmetics');
              $message->from('marketing@eminacosmetics.com', 'Emina Cosmetics');
              $message->to($consumerdata['email']);
          });
          return redirect('/voucher-alt');

          // return back()->with('alert-success','Berhasil Kirim Email');
      }
      catch (Exception $e){
          return response (['status' => false,'errors' => $e->getMessage()]);
      }
    }


    public function testEmail()
    {
      $consumerdata = "Belva Prima";
      $parameter = "Test Email";
      try{
          // $pesan = "eminadaq.local/".$

          // Mail::send('email', ['nama' => $request->name, 'pesan' => $request->pesan], function ($message) use ($request)
          Mail::send('email', ['nama' => "Belva Prima", 'pesan' => $parameter], function ($message) use ($consumerdata)
          {
              $message->subject('Emina Cosmetics');
              $message->from('marketing@eminacosmetics.com', 'Emina Cosmetics');
              $message->to('belvaprima105@gmail.com');
          });
          return redirect('/voucher-alt');

          // return back()->with('alert-success','Berhasil Kirim Email');
      }
      catch (Exception $e){
          return response (['status' => false,'errors' => $e->getMessage()]);
      }
    }


}
