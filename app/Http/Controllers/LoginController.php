<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Illuminate\Support\Facades\Session;


class LoginController extends Controller
{
  public function view() {
    return view('login.login');
  }

  public function login(Request $request) {
    if($request->isMethod('post')){
      $data = $request->input();
      // dd($data);
      // if(Auth::attempt(['email'=>$data['email'],'password'=>$data['password']])){
      if($data['email']=="admin@admin.com" && $data['password']=="ptidotnet"){
        return redirect('/dashboard');
      }else{
        return redirect('/login')->with('flash_message_error', 'Invalid username or password');
      }
    }
    return view('login.login');
  }

  public function logout() {
    Session::flush();
    return redirect ('/')->with('flash_message_success_logout', 'Logged Out Successfully');
  }
}
