<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumerInstagram extends Model
{
  protected $fillable =[
    'account',
    'follower',
    'following',
    'posts',
  ];

  public function get_consumer() {

    return $this->belongsTo('App\Consumer', 'account', 'ig-account');

  }

}
