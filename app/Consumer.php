<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consumer extends Model
{
    protected $fillable =[
      'name',
      'dob',
      'gender',
      'email',
      'phone',
      'ig-account',
      'domisili',
      'what-you-do',
      'money-spent',
      'provinsi',
      'kota'
    ];

    public function get_skin_profile() {

      return $this->hasOne('App\ConsumerSkinProfile', 'email', 'email');

    }

    public function get_instagram() {

      return $this->hasOne('App\ConsumerSkinProfile', 'account', 'ig-account');

    }


}
