<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SourceInformation extends Model
{
  protected $fillable =[
    'email',
    'how-you-know',
  ];
}
