<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EminaProductHave extends Model
{
  protected $fillable =[
    'email',
    'which-do-you-have',
  ];
}
