<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MustHaveProduct extends Model
{
  protected $fillable =[
    'email',
    'must-have',
  ];
}
