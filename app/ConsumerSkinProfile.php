<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumerSkinProfile extends Model
{
  protected $fillable =[
    'email',
    'phone',
    'skin-type',
    'skin-concern',
    'skin-tone',
    'undertone',
  ];

  public function get_consumer() {

    return $this->belongsTo('App\Consumer', 'email', 'email');

  }


}
