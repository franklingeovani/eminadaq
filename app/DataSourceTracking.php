<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataSourceTracking extends Model
{
  protected $fillable =[
    'email',
    'medium',
    'source',
    'campaign',
  ];
}
